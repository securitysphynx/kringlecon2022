//Making a list
console.log("Making a list...")
const santasList = [
    {childName: 'Alice', nice: 'Yes', location: 'NY'},
    {childName: 'Bob', nice: 'Yes', location: 'AK'},
    {childName: 'Chantel', nice: 'No', location: 'WY'},
    {childName: 'Damien', nice: 'Yes', location: 'VA'},
    {childName: 'Ezra', nice: 'Yes', location: 'LA'},
    {childName: 'Felice', nice: 'Yes', location: 'AK'},
    {childName: 'Gretchen', nice: 'Yes', location: 'AL'},
    {childName: 'Hayden', nice: 'Yes', location: 'NJ'},
    {childName: 'Inez', nice: 'Yes', location: 'NY'},
    {childName: 'Javier', nice: 'Yes', location: 'CO'},
    {childName: 'Kristen', nice: 'No', location: 'DE'},
    {childName: 'Luciano', nice: 'Yes', location: 'WA'},
    {childName: 'Melissa', nice: 'Yes', location: 'WV'},
    {childName: 'Nathan', nice: 'Yes', location: 'WV'},
    {childName: 'Oscar', nice: 'Yes', location: 'NY'},
    {childName: 'Paul', nice: 'Yes', location: 'CO'},
    {childName: 'Quincy', nice: 'No', location: 'AK'},
    {childName: 'Ravi', nice: 'Yes', location: 'DE'},
    {childName: 'Simon', nice: 'Yes', location: 'TN'},
    {childName: 'Trey', nice: 'No', location: 'KY'},
    {childName: 'Umar', nice: 'Yes', location: 'KY'},
    {childName: 'Viktor', nice: 'Yes', location: 'VA'},
    {childName: 'Whitney', nice: 'Yes', location: 'CO'},
    {childName: 'Xavier', nice: 'Yes', location: 'DE'},
    {childName: 'Yelena', nice: 'Yes', location: 'TN'},
    {childName: 'Zander', nice: 'Yes', location: 'WV'}
];

//Check the list twice
santasList.forEach(function(listCheck1) {
    console.log(listCheck1)
});
console.log("Checking it twice...")
santasList.forEach(function(listCheck2) {
    console.log(listCheck2)
});
//Gonna find out who's naughty or...
console.log("NAUGHTY LIST")
santasList.forEach((santasList) => {
    if(santasList.nice == 'No'){
        console.log(santasList.childName)
    }
});
//.... nice
console.log("NICE LIST")
santasList.forEach((santasList) => {
    if(santasList.nice == 'Yes'){
        console.log(santasList.childName)
    }
});
